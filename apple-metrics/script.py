import os
import subprocess
import json
import requests
import schedule
import time

MAC_HOST = os.getenv("MAC_HOST", "10.248.0.60")
MAC_USER = os.getenv("MAC_USER", "netsadmin")
MAC_PASSWORD = os.getenv("MAC_PASSWORD")
VICTORIA_METRICS_URL = os.getenv("VICTORIA_METRICS_URL", "http://10.248.0.103:8428/api/v1/import/prometheus")
VICTORIA_METRICS_USERNAME = os.getenv("VICTORIA_METRICS_USERNAME", "zabbix")
VICTORIA_METRICS_PASSWORD = os.getenv("VICTORIA_METRICS_PASSWORD")
EVENT_NAME = os.getenv("EVENT_NAME")

def log_environment_variables():
    print(f"MAC_HOST: {MAC_HOST}")
    print(f"MAC_USER: {MAC_USER}")
    print(f"MAC_PASSWORD: {MAC_PASSWORD}")
    print(f"VICTORIA_METRICS_URL: {VICTORIA_METRICS_URL}")
    print(f"VICTORIA_METRICS_USERNAME: {VICTORIA_METRICS_USERNAME}")
    print(f"VICTORIA_METRICS_PASSWORD: {VICTORIA_METRICS_PASSWORD}")
    print(f"EVENT_NAME: {EVENT_NAME}")

    if not all([MAC_HOST, MAC_USER, MAC_PASSWORD, VICTORIA_METRICS_URL, VICTORIA_METRICS_USERNAME, VICTORIA_METRICS_PASSWORD]):
        print("Error: One or more environment variables are not set.")
        exit(1)

def fetch_and_send_metrics():
    print("Execution of fetch_and_send_metrics")

    ssh_command = f"sshpass -p {MAC_PASSWORD} ssh -o StrictHostKeyChecking=no {MAC_USER}@{MAC_HOST} AssetCacheManagerUtil status -j 2>/dev/null"
    try:
        output = subprocess.check_output(ssh_command, shell=True, text=True)
        result = json.loads(output)
    except subprocess.CalledProcessError as e:
        print(f"Error when executing the SSH command: {e}")
        return
    except json.JSONDecodeError as e:
        print(f"Error parsing the JSON output: {e}")
        return

    ache = result["result"].get("Active")
    cachestatus = result["result"].get("CacheStatus")
    regstatus = result["result"].get("RegistrationStatus")
    actualcacheused = result["result"].get("ActualCacheUsed")
    cacheicloud = result["result"].get("CacheDetails", {}).get("iCloud")
    cacheios = result["result"].get("CacheDetails", {}).get("iOS Software")
    cachemac = result["result"].get("CacheDetails", {}).get("Mac Software")
    cacheother = result["result"].get("CacheDetails", {}).get("Other")
    cachefree = result["result"].get("CacheFree")
    cacheused = result["result"].get("CacheUsed")
    cachelimit = result["result"].get("CacheLimit")
    lasthour = result["result"].get("MaxCachePressureLast1Hour")
    returnedtoclients = result["result"].get("TotalBytesReturnedToClients")
    storedfromorgin = result["result"].get("TotalBytesStoredFromOrigin")
    bytesimported = result["result"].get("TotalBytesImported")
    bytessince = result["result"].get("TotalBytesAreSince")

    metrics = f"""
    apple_cache_cache_active{{event_name="{EVENT_NAME}", server="{MAC_HOST}"}} {ache}
    apple_cache_status{{event_name="{EVENT_NAME}", server="{MAC_HOST}"}} {cachestatus}
    apple_cache_registrationstatus{{event_name="{EVENT_NAME}", server="{MAC_HOST}"}} {regstatus}
    apple_cache_actual_cache_used{{event_name="{EVENT_NAME}", server="{MAC_HOST}"}} {actualcacheused}
    apple_cache_cache_used_icloud{{event_name="{EVENT_NAME}", server="{MAC_HOST}"}} {cacheicloud}
    apple_cache_cache_used_ios{{event_name="{EVENT_NAME}", server="{MAC_HOST}"}} {cacheios}
    apple_cache_cache_used_mac{{event_name="{EVENT_NAME}", server="{MAC_HOST}"}} {cachemac}
    apple_cache_cache_used_other{{event_name="{EVENT_NAME}", server="{MAC_HOST}"}} {cacheother}
    apple_cache_cache_free{{event_name="{EVENT_NAME}", server="{MAC_HOST}"}} {cachefree}
    apple_cache_cache_total_used{{event_name="{EVENT_NAME}", server="{MAC_HOST}"}} {cacheused}
    apple_cache_cache_max_limit{{event_name="{EVENT_NAME}", server="{MAC_HOST}"}} {cachelimit}
    apple_cache_MaxCachePressureLast1Hour{{event_name="{EVENT_NAME}", server="{MAC_HOST}"}} {lasthour}
    apple_cache_returned_to_clients{{event_name="{EVENT_NAME}", server="{MAC_HOST}"}} {returnedtoclients}
    apple_cache_stored_from_origin{{event_name="{EVENT_NAME}", server="{MAC_HOST}"}} {storedfromorgin}
    apple_cache_bytes_imported{{event_name="{EVENT_NAME}", server="{MAC_HOST}"}} {bytesimported}
    apple_cache_bytes_since{{event_name="{EVENT_NAME}", server="{MAC_HOST}"}} {bytessince}
    """

    response = requests.post(VICTORIA_METRICS_URL, data=metrics, auth=(VICTORIA_METRICS_USERNAME, VICTORIA_METRICS_PASSWORD))
    if response.status_code == 204:
        print("Metrics sent successfully.")
    else:
        print(f"Error when sending the metrics: {response.status_code}, {response.text}")


log_environment_variables()

schedule.every(1).minute.do(fetch_and_send_metrics)

fetch_and_send_metrics()

while True:
    schedule.run_pending()
    time.sleep(1)
