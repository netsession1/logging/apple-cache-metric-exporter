# Apple-Cache-Metrics

Docker Container um verschiedene Metriken von einem Apple Cache Server über SSH zu sammeln und diese Metriken zu Überwachungs- und Visualisierungszwecken an eine Victoria Metrics Datenbank zu senden.

## Getting started

docker compose minimal 

```
---
services:
  apple-cache-exporter:
    image: registry.gitlab.com/netsession1/logging/apple-cache-metric-exporter/apple-metrics-main:latest
    environment:
      MAC_PASSWORD: "some_password"
      VICTORIA_METRICS_PASSWORD: "some_password"
      EVENT_NAME: "Netsession"
```

## Service Setup

| Variable | Default | Description |
| -- | -- | -- |
| MAC_HOST | 10.248.0.60 | Mac IP Address |
| MAC_USER | netsadmin | Mac Username |
| MAC_PASSWORD | | Mac Password |
| VICTORIA_METRICS_URL | http://10.248.0.103:8428/api/v1/import/prometheus | Victoria Metrics DB Url |
| VICTORIA_METRICS_USERNAME | zabbix | Victoria Metrics Username |
| VICTORIA_METRICS_PASSWORD | | Victoria Metrics Password |
| EVENT_NAME | | Short Event Name e.g. Netsession |

## Information that will be exported 

    Active
    CacheStatus
    RegistrationStatus
    ActualCacheUsed
    CacheDetails.iCloud
    CacheDetails."iOS Software"
    CacheDetails."Mac Software"
    CacheDetails.Other
    CacheFree
    CacheUsed
    CacheLimit
    MaxCachePressureLast1Hour
    TotalBytesReturnedToClients
    TotalBytesStoredFromOrigin
    TotalBytesImported
    TotalBytesAreSince
